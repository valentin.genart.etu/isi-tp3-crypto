1. Chaque responsable possède une clé USB `usb0` et `usb1` sur laquelle est stockée une clé chiffrée dans un fichier `key`. Chaque utilisateur a un mot de passe que lui seul connaît, permettant de déchiffrer cette clé. Les deux clés déchiffrées permettent de générer une clé de déchiffrage pour la clé "maîtresse", stockée dans le ramdisk. La clé "maîtresse" est stockée chiffrée sur le disque dur. Cette dernière, une fois déchiffrée par la clé générée, permet de déchiffrer le fichier bancaire.

2. Le script mise_en_service demande le mot de passe de chaque responsable et et déchiffre leurs clés avec ces mots de passe (les mots de passe sont "toto" et "tata" respectivement). Il génère la clé de déchiffrage de la masterkey qu'il stocke dans le ramdisk (en clair), et déchiffre la masterkey avec cette clé qu'il stocke dans le ramdisk en clair aussi (le contenu du ramdisk est inaccessible).  
Le script add_pair demande un nom et une carte bancaire et stocke cette paire dans le fichier bancaire chiffré.  
Le script rm_pair demande un nom et une carte bancaire et supprime cette paire du fichier bancaire si elle existe.  
Le script search_cards demande un nom et affiche toutes les cartes associées à ce nom s'il y en a.  
Chacun de ces 3 derniers scripts vérifient que le service a bien été lancé pour effectuer leurs opérations.  
Il est essentiel de supprimer les deux clés stockées en ramdisk à la fin des opérations (fermer le service).

3. En passant de 2 clés USB à 4 clés USB possible, on passe d'une combinaison possible à 4 combinaisons. 
Nous savons que : 
- Chaque clé USB contient une clé de chiffrement unique. La concatenation des deux clés génère une clé de déchiffrement unique.
- La clé permettant de décrypter le fichier bancaire est unique
**Comment pouvons-nous décrypter la master key chiffrée avec 4 combinaisons possibles de clé de déchiffrement ?**

Pour ce faire, il nous faut créer 1 Master Key chiffrée pour chaque combinaison de clés USB (01, 03, 21, 23 sachant que 0=resp1, 1=resp2, 2=remp1, 3=remp2).
Lorsqu'on concatène les deux clés de chiffrement des USB, cela nous donne une clé de déchiffrement. Cette clé va essayer de déchiffrer toutes les master key chiffrées (4) jusqu'à ce qu'elle réussisse. Si elle ne réussie pas, alors cela sous-entend que la combinaison des clés USB est incorrecte. 
Si elle réussit, le déchiffrement de la master key chiffrée nous donne une unique clé permettant de décrypter le fichier bancaire.

![Image](./q3.PNG)

5. Le script repudiation permet de supprimer les droits d'acces à une personne. Pour cela, il faut que le dossier usb correspondant soit supprimé (ce qui simule le fait que la clé n'est pas branchée) puis lancer le script. Les mots de passe des trois autres responsables sont demandés, et s'ils sont correct, alors les deux masterkey cryptées associées au quatrième responsable sont supprimées (ex: pour le resp 2, les masterkey 21 et 23 sont supprimées), ce qui fait qu'il ne pourra plus se connecter au serveur.  
Problèmes avec cette méthode: Possibilité de supprimer un seul utilisateur uniquement puisque le script exige 3 mots de passe. Impossible de remplacer l'utilisateur supprimé par un autre puisque les masterkey sont supprimées.


